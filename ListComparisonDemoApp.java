/*


import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


*/

import java.util.*; // wildcard import of all classes and interfaces in java.util


/**
 * Application class to demonstrate that the performance of an ArrayList and
 * that of a LinkedList appear to be congruent with theory
 * @author Brandon
 * @version 146sp18_hw4ex1
 */
public class ListComparisonDemoApp {
    
    /**
     * Main method for the app
     * @param args
     */
    public static void main( String[] args )
    {
        String[] myStrings = {"zero", "one", "two", "three", "four" };
        
        System.out.println("Original array of strings: " + 
                Arrays.toString( myStrings ) + "\n" );
        
        // Create two List objects (one ArrayLsit, one LinkedList) that
        // both contain the same data
        List<String> myArrayList = new ArrayList<>( Arrays.asList( myStrings ) );
        List<String> myLinkedList = new LinkedList<>( Arrays.asList( myStrings ) );
        
        // Use System.nanoTime() to help the elapsed time
        // it takes to execute a section of code
        
        long startTime = System.nanoTime();
        myArrayList.add(2, "two");
        long endTime = System.nanoTime();
        long elapsedTime = endTime - startTime;
        
        System.out.println("Elapsed time to insert element in the middle" +
                " of an ArrayList: " + elapsedTime );
        
        myLinkedList.add( 2, "two" );
        
        startTime = System.nanoTime();
        myLinkedList.add(2, "two");
        endTime = System.nanoTime();
        elapsedTime = endTime - startTime;
        
        System.out.println("Elapsed time to insert element in the middle" +
                " of a LinkedList: " + elapsedTime );
        
        
        System.out.println( myArrayList );
        System.out.println( myLinkedList );
        
        String middleElement = "";
        
        startTime = System.nanoTime();
        middleElement = myArrayList.get(2); // retrieves element at index 2
        endTime = System.nanoTime();
        elapsedTime = endTime - startTime;
        
        System.out.println("Elapsed time to access element in the middle" +
                " of myArrayList: " + elapsedTime );
        
        
        
        
    } // end method main
    
    
}  // end class ListComparisonDemoApp
